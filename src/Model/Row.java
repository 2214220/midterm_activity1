package Model;

public class Row {
    int customerNumber;
    int interarrivalTime;
    int arrivalTime;
    int serviceTime;
    int timeServiceBegins;
    int waitingTime;
    int timeServiceEnds;
    int timeCustomerSpendsSystem;
    int idleTime;

    public int getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public int getInterarrivalTime() {
        return interarrivalTime;
    }

    public void setInterarrivalTime(int interarrivalTime) {
        this.interarrivalTime = interarrivalTime;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public int getTimeServiceBegins() {
        return timeServiceBegins;
    }

    public void setTimeServiceBegins(int timeServiceBegins) {
        this.timeServiceBegins = timeServiceBegins;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public int getTimeServiceEnds() {
        return timeServiceEnds;
    }

    public void setTimeServiceEnds(int timeServiceEnds) {
        this.timeServiceEnds = timeServiceEnds;
    }

    public int getTimeCustomerSpendsSystem() {
        return timeCustomerSpendsSystem;
    }

    public void setTimeCustomerSpendsSystem(int timeCustomerSpendsSystem) {
        this.timeCustomerSpendsSystem = timeCustomerSpendsSystem;
    }

    public int getIdleTime() {
        return idleTime;
    }

    public void setIdleTime(int idleTime) {
        this.idleTime = idleTime;
    }

    public Row(int customerNumber, int interarrivalTime, int arrivalTime,
               int serviceTime, int timeServiceBegins, int waitingTime,
               int timeServiceEnds, int timeCustomerSpendsSystem, int idleTime){
        this.customerNumber = customerNumber;
        this.interarrivalTime = interarrivalTime;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
        this.timeServiceBegins = timeServiceBegins;
        this.waitingTime = waitingTime;
        this.timeServiceEnds = timeServiceEnds;
        this.timeCustomerSpendsSystem = timeCustomerSpendsSystem;
        this.idleTime = idleTime;

    }

    public String toString(){
        return String.format("%15s %15s %15s %15s %15s %15s %15s %15s %15s \n", "CustNo: " + customerNumber, "IAT: " + interarrivalTime, "AT: " + arrivalTime, "ST: " + serviceTime, "TSB: " +
                timeServiceBegins, "WT: " + waitingTime, "TSE: " + timeServiceEnds, "TCSS: " + timeCustomerSpendsSystem  , "IT: " +
                idleTime);
    }

    public String[] toStringArray(){
        return new String[] {String.valueOf(customerNumber), String.valueOf(interarrivalTime), String.valueOf(arrivalTime), String.valueOf(serviceTime),
        String.valueOf(timeServiceBegins), String.valueOf(waitingTime), String.valueOf(timeServiceEnds), String.valueOf(timeCustomerSpendsSystem),
        String.valueOf(idleTime)};
    }
}
