package Model;

import View.GraphicalInterface;

import java.util.ArrayList;
import java.util.Random;

public class ModelFunction {

    private static ModelFunction modelFunctionInstance = null;

    private static ArrayList<Row> rows;

    public static ModelFunction initialize(){
        if(modelFunctionInstance == null){
            modelFunctionInstance = new ModelFunction();
        }
        return modelFunctionInstance;
    }

    public String[][] processFunction(String chosen, int number){
        int noCustomer, noMinutes, custNo, interarrivalTime, arrivalTime = 0,
            serviceTime, timeServiceBegins, waitingTime, timeServiceEnds,
            timeCustomerSpendsSystem, idleTime;
        rows = new ArrayList<>();
        Row rowan;

        if(chosen.equalsIgnoreCase("customers")){
            noCustomer = number;

            for(int i = 1; i <= noCustomer; i++){
                if (rows.isEmpty()){
                    custNo = i;
                    interarrivalTime = 0;
                    serviceTime = getServiceTime(generateRandomInt());
                    timeServiceBegins = 0;
                    waitingTime = 0;
                    timeServiceEnds = timeServiceBegins + serviceTime;
                    timeCustomerSpendsSystem = timeServiceEnds - arrivalTime;
                    idleTime = 0;
                    rowan = new Row(custNo, interarrivalTime, arrivalTime, serviceTime, timeServiceBegins, waitingTime,
                            timeServiceEnds, timeCustomerSpendsSystem, idleTime);
                    //
                    // GraphicalInterface.initialize().getSimulationTextArea().append(rowan.toString());
                    rows.add(rowan);
                } else {
                    custNo = i;
                    interarrivalTime = getInterarrivalTime(generateRandomDouble());
                    arrivalTime = rows.get(rows.size() - 1).arrivalTime + interarrivalTime;
                    serviceTime = getServiceTime(generateRandomInt());
                    timeServiceBegins = Math.max(rows.get(rows.size() - 1).timeServiceEnds, arrivalTime);
                    waitingTime = timeServiceBegins - arrivalTime;
                    timeServiceEnds = timeServiceBegins + serviceTime;
                    timeCustomerSpendsSystem = timeServiceEnds - arrivalTime;
                    if (arrivalTime > rows.get(rows.size() - 1).timeServiceEnds){
                        idleTime = arrivalTime - rows.get(rows.size() - 1).timeServiceEnds;
                    } else {
                        idleTime = 0;
                    }
                    rowan = new Row(custNo, interarrivalTime, arrivalTime, serviceTime, timeServiceBegins, waitingTime,
                            timeServiceEnds, timeCustomerSpendsSystem, idleTime);
                    //GraphicalInterface.initialize().getSimulationTextArea().append(rowan.toString());
                    rows.add(rowan);
                }
            }
        } else if(chosen.equalsIgnoreCase("minutes")){
            noMinutes = number;
            int i = 1;
            do {
                if (rows.isEmpty()){
                    custNo = 1;
                    interarrivalTime = 0;
                    serviceTime = getServiceTime(generateRandomInt());
                    timeServiceBegins = 0;
                    waitingTime = 0;
                    timeServiceEnds = timeServiceBegins + serviceTime;
                    timeCustomerSpendsSystem = timeServiceEnds - arrivalTime;
                    idleTime = 0;
                    rowan = new Row(custNo, interarrivalTime, arrivalTime, serviceTime, timeServiceBegins, waitingTime,
                            timeServiceEnds, timeCustomerSpendsSystem, idleTime);
                    //GraphicalInterface.initialize().getSimulationTextArea().append(rowan.toString());
                    rows.add(rowan);
                    i++;
                } else {
                    custNo = i;
                    interarrivalTime = getInterarrivalTime(generateRandomDouble());
                    arrivalTime = rows.get(rows.size() - 1).arrivalTime + interarrivalTime;
                    serviceTime = getServiceTime(generateRandomInt());
                    timeServiceBegins = Math.max(rows.get(rows.size() - 1).timeServiceEnds, arrivalTime);
                    waitingTime = timeServiceBegins - arrivalTime;
                    timeServiceEnds = timeServiceBegins + serviceTime;
                    timeCustomerSpendsSystem = timeServiceEnds - arrivalTime;
                    if (arrivalTime > rows.get(rows.size() - 1).timeServiceEnds){
                        idleTime = arrivalTime - rows.get(rows.size() - 1).timeServiceEnds;
                    } else {
                        idleTime = 0;
                    }
                    rowan = new Row(custNo, interarrivalTime, arrivalTime, serviceTime, timeServiceBegins, waitingTime,
                            timeServiceEnds, timeCustomerSpendsSystem, idleTime);
                    //GraphicalInterface.initialize().getSimulationTextArea().append(rowan.toString());
                    rows.add(rowan);
                    i++;
                }
            } while (timeServiceEnds < noMinutes);
        }
        return convertObjectListToArray(rows);
    }

    public static int getInterarrivalTime(double random){
        int interarrivalTime;

        if (random < 0.124){
            interarrivalTime = 1;
        } else if(random < 0.249){
            interarrivalTime = 2;
        } else if(random < 0.374){
            interarrivalTime = 3;
        } else if(random < 0.499){
            interarrivalTime = 4;
        } else if(random < 0.624){
            interarrivalTime = 5;
        } else if(random < 0.749){
            interarrivalTime = 6;
        } else if(random < 0.874){
            interarrivalTime = 7;
        } else {
            interarrivalTime = 8;
        }

        return interarrivalTime;
    }

    public static int getServiceTime(int random){
        int serviceTime;
        if (random < 15){
            serviceTime = 1;
        } else if (random < 45){
            serviceTime = 2;
        } else if (random < 70){
            serviceTime = 3;
        } else if (random < 90){
            serviceTime = 4;
        } else {
            serviceTime = 5;
        }
        return serviceTime;
    }

    public static double generateRandomDouble(){
        Random rand = new Random();
        return rand.nextDouble();
    }

    public static int generateRandomInt(){
        Random rand = new Random();
        return rand.nextInt(99);
    }


    public double computeAverageWaitingTime(){
        double totalWaitingTime = 0;
        for (Row row: rows){
            totalWaitingTime = totalWaitingTime+row.getWaitingTime();
        }
        return totalWaitingTime/rows.size();
    }

    public double computeQueueProbability(){
        int customersWhoWaited = 0;
        for (Row row: rows){
            if (row.waitingTime > 0){
                customersWhoWaited++;
            }
        }
        return customersWhoWaited/((double)rows.size())*100;
    }

    public double computeServerIdleTime(){
        double totalIdleTime = 0;

        for (Row row: rows){
            totalIdleTime = totalIdleTime + row.getIdleTime();
        }
        return (totalIdleTime/ rows.get(rows.size() - 1).getTimeServiceEnds())*100;
    }

    public double computeAverageServiceTime(){
        double totalServiceTime = 0;

        for (Row row: rows){
            totalServiceTime = totalServiceTime + row.getServiceTime();
        }

        return totalServiceTime/rows.size();
    }

    public double computeAverageIAT(){
        double totalIAT = 0;
        for(Row row: rows){
            totalIAT = totalIAT+row.getInterarrivalTime();
        }
        return totalIAT/rows.size();
    }

    public double computeAverageWaitingTimeInQueue(){
        int peopleWhoWaited = 0;
        double totalWaitingTime = 0;
        for(Row row: rows){
            if(row.getWaitingTime()>0){
                peopleWhoWaited++;
                totalWaitingTime = totalWaitingTime + row.getWaitingTime();
            }
        }
        return totalWaitingTime/peopleWhoWaited;
    }

    public double computeAverageTimeInSystem(){
        double totalTimeInSystem = 0;
        for(Row row: rows){
            totalTimeInSystem = totalTimeInSystem + row.getTimeCustomerSpendsSystem();
        }
        return totalTimeInSystem/rows.size();
    }

    public String[][] convertObjectListToArray(ArrayList<Row> objectList){
        String[][] string2DArray = new String[objectList.size()][9];

        for (int i = 0; i < string2DArray.length; i++){
            string2DArray[i][0] = String.valueOf(objectList.get(i).getCustomerNumber());
            string2DArray[i][1] = String.valueOf(objectList.get(i).getInterarrivalTime());
            string2DArray[i][2] = String.valueOf(objectList.get(i).getArrivalTime());
            string2DArray[i][3] = String.valueOf(objectList.get(i).getServiceTime());
            string2DArray[i][4] = String.valueOf(objectList.get(i).getTimeServiceBegins());
            string2DArray[i][5] = String.valueOf(objectList.get(i).getWaitingTime());
            string2DArray[i][6] = String.valueOf(objectList.get(i).getTimeServiceEnds());
            string2DArray[i][7] = String.valueOf(objectList.get(i).getTimeCustomerSpendsSystem());
            string2DArray[i][8] = String.valueOf(objectList.get(i).getIdleTime());
        }
        return string2DArray;
    }
}