package Controller;

/**
 * Group Members
 * Adan, Ramdale
 * Bernabe, Rebelais
 * Codangos, Wladeck
 * Malecdan, Leindell
 */

import View.GraphicalInterface;
import Model.ModelFunction;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

public class Simulation implements ActionListener {

    private static final GraphicalInterface GUI = GraphicalInterface.initialize();
    private static final ModelFunction model = ModelFunction.initialize();

    private static String choice;

    public static void main(String[] args) {
        Simulation simulation = new Simulation();
        simulation.run();
    }

    public void run(){
        GUI.createFrame(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        DecimalFormat df = new DecimalFormat("###.####");
        try{
        if (e.getSource().equals(GUI.getCustomersChoiceBtn())){
            GUI.getMainFrame().setSize(900, 700);
            GUI.getMainFrame().setLocationRelativeTo(null);
            GUI.getMaxLabel().setText("Max Customers:");
            GUI.getMaxLabel().setFont(new Font("Helvetica", Font.BOLD, 20));
            choice = "customers";
            GUI.getMainPanel().removeAll();
            GUI.getMainPanel().add(GUI.getTopPanel(), BorderLayout.NORTH);
            GUI.getMainPanel().repaint();
            GUI.getMainPanel().revalidate();
        }
        else if(e.getSource().equals(GUI.getMinutesChoiceBtn())){
            GUI.getMainFrame().setSize(900, 700);
            GUI.getMainFrame().setLocationRelativeTo(null);
            GUI.getMaxLabel().setText("Max Minutes:");
            GUI.getMaxLabel().setFont(new Font("Helvetica", Font.BOLD, 20));
            choice = "minutes";
            GUI.getMainPanel().removeAll();
            GUI.getMainPanel().add(GUI.getTopPanel(), BorderLayout.NORTH);
            GUI.getMainPanel().repaint();
            GUI.getMainPanel().revalidate();
        }

        else if(e.getSource().equals(GUI.getStartButton())){
            GUI.setRows(model.processFunction(choice, Integer.parseInt(GUI.getMaxTextArea().getText())));
            GUI.getMainPanel().add(GUI.simulationPanel(), BorderLayout.CENTER);
            GUI.getMainPanel().add(GUI.computationsPanel(), BorderLayout.SOUTH);
            GUI.getMainPanel().repaint();
            GUI.getMainPanel().revalidate();
            GUI.getSimulationTextArea().append("COMPUTATIONS: \n\n");
            GUI.getSimulationTextArea().append("Average Waiting Time: "+ df.format(model.computeAverageWaitingTime())+" minutes"+"\n");
            GUI.getSimulationTextArea().append("Probability of customer to wait in queue: "+ df.format(model.computeQueueProbability())+"\n");
            GUI.getSimulationTextArea().append("Proportion of idle time of the server: "+ df.format(model.computeServerIdleTime())+"\n");
            GUI.getSimulationTextArea().append("Average Service Time: "+ df.format(model.computeAverageServiceTime())+" minutes"+"\n");
            GUI.getSimulationTextArea().append("Average Inter-arrival Time: "+ df.format(model.computeAverageIAT())+" minutes"+"\n");
            GUI.getSimulationTextArea().append("Average time for those who wait in queue: "+ df.format(model.computeAverageWaitingTimeInQueue())+" minutes"+"\n");
            GUI.getSimulationTextArea().append("Average time a customer spends in the system: "+ df.format(model.computeAverageTimeInSystem())+" minutes"+"\n \n");
            GUI.getSimulationTextArea().append("Adan, Ramdale       Bernabe, Rebelais       Codangos, Wladeck       Malecdan, Leindell");
            GUI.getMaxTextArea().setEnabled(false);
            GUI.getStartButton().setEnabled(false);
        }

        else if(e.getSource().equals(GUI.getSimulateAgainBtn())){
            GUI.getMainFrame().dispose();
            GUI.createFrame(this);
        }
        }catch (Exception exception){
            JOptionPane.showMessageDialog(null,"Invalid Input");
        }
    }
}