package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class GraphicalInterface {

    private static JFrame mainFrame;
    private static JPanel mainPanel;
    private static JPanel topPanel;
    private static JPanel choicePanel;
    private static JPanel computationsPanel;
    private static JTextArea maxTextArea;
    private static JLabel maxLabel;
    private static JButton startButton;
    private static JButton customersChoiceBtn;
    private static JButton minutesChoiceBtn;
    private static JButton simulateAgainBtn;
    private static JScrollPane scrollPane;
    private static JTextArea simulationTextArea;
    private static JTable simulationTable;
    private static String[][] rows;
    private static final String[] column = {"Customer Number", "Interarrival Time", "Arrival Time", "Service Time", "Time Service Begins",
    "Waiting Time", "Time Service Ends", "Time Customer spends in System", "Idle Time"};

    private static GraphicalInterface interfaceInstance = null;

    public static GraphicalInterface initialize(){
        if(interfaceInstance == null){
            interfaceInstance = new GraphicalInterface();
        }
        return interfaceInstance;
    }

    public void createFrame(ActionListener actionListener) {
        mainFrame = new JFrame();
        mainPanel = new JPanel();
        mainFrame.setSize(600, 200);
        mainFrame.setResizable(false);
        mainFrame.setLocationRelativeTo(null);

        maxLabel = new JLabel("Maximum");
        maxLabel.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        maxTextArea = new JTextArea();
        maxTextArea.setColumns(10);

        startButton = new JButton("Start Simulation");
        startButton.setFocusPainted(false);
        startButton.setBackground(Color.decode("#3d5a80"));
        startButton.setForeground(Color.WHITE);
        startButton.addActionListener(actionListener);

        simulateAgainBtn = new JButton("Simulate another");
        simulateAgainBtn.setFocusPainted(false);
        simulateAgainBtn.setBackground(Color.decode("#3d5a80"));
        simulateAgainBtn.setForeground(Color.WHITE);
        simulateAgainBtn.addActionListener(actionListener);

        topPanel = new JPanel();
        topPanel.setPreferredSize(new Dimension(600, 50));
        topPanel.setBackground(Color.decode("#98c1d9"));
        topPanel.add(maxLabel);
        topPanel.add(maxTextArea);
        topPanel.add(startButton);
        topPanel.add(simulateAgainBtn);

        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(choicePanel(actionListener), BorderLayout.CENTER);
        mainFrame.setContentPane(mainPanel);
        mainFrame.setVisible(true);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
    public JScrollPane simulationPanel(){
        simulationTable = new JTable(rows, column);
        simulationTable.setBounds(50, 50, 500, 250);
        simulationTable.setBackground(Color.decode("#e0fbfc"));
        simulationTable.setDragEnabled(false);
        simulationTable.setEnabled(false);
        scrollPane = new JScrollPane(simulationTable);
        scrollPane.setPreferredSize(new Dimension(559, 300));
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        return scrollPane;
    }

    public JPanel computationsPanel(){
        simulationTextArea = new JTextArea();
        simulationTextArea.setFont(new Font("Helvetica", Font.PLAIN, 15));
        simulationTextArea.setPreferredSize(new Dimension(450, 250));
        simulationTextArea.setEditable(false);
        computationsPanel = new JPanel();
        computationsPanel.setSize(500, 300);
        computationsPanel.setLayout(new BorderLayout());
        computationsPanel.setVisible(true);
        computationsPanel.add(simulationTextArea);
        return computationsPanel;
    }

    public JPanel choicePanel(ActionListener actionListener){
        choicePanel = new JPanel();
        choicePanel.setSize(new Dimension(500, 500));
        JLabel label = new JLabel("Choose the stopping criteria for the simulation");
        label.setFont(new Font("Helvetica", Font.PLAIN, 20));

        minutesChoiceBtn = new JButton("Minutes");
        minutesChoiceBtn.setPreferredSize(new Dimension(200, 100));
        minutesChoiceBtn.setFocusPainted(false);
        minutesChoiceBtn.setBackground(Color.decode("#3d5a80"));
        minutesChoiceBtn.setFont(new Font("Helvetica", Font.BOLD, 30));
        minutesChoiceBtn.setForeground(Color.WHITE);
        minutesChoiceBtn.addActionListener(actionListener);

        customersChoiceBtn = new JButton("Customers");
        customersChoiceBtn.setPreferredSize(new Dimension(200, 100));
        customersChoiceBtn.setFocusPainted(false);
        customersChoiceBtn.setBackground(Color.decode("#3d5a80"));
        customersChoiceBtn.setForeground(Color.WHITE);
        customersChoiceBtn.setFont(new Font("Helvetica", Font.BOLD, 30));
        customersChoiceBtn.addActionListener(actionListener);

        choicePanel.add(label);
        choicePanel.add(customersChoiceBtn);
        choicePanel.add(minutesChoiceBtn);
        return choicePanel;
    }

    public JTextArea getMaxTextArea() {
        return maxTextArea;
    }

    public JLabel getMaxLabel() {
        return maxLabel;
    }

    public JTable getSimulationTable() { return simulationTable; }

    public JButton getStartButton() {
        return startButton;
    }

    public JButton getSimulateAgainBtn(){ return simulateAgainBtn; }

    public JButton getCustomersChoiceBtn() {
        return customersChoiceBtn;
    }

    public JButton getMinutesChoiceBtn() {
        return minutesChoiceBtn;
    }

    public GraphicalInterface getInterfaceInstance() {
        return interfaceInstance;
    }

    public JFrame getMainFrame(){
        return mainFrame;
    }

    public JPanel getMainPanel(){
        return mainPanel;
    }

    public JPanel getTopPanel(){
        return topPanel;
    }

    public JPanel getChoicePanel() { return choicePanel; }

    public JScrollPane getScrollPane(){
        return scrollPane;
    }

    public JTextArea getSimulationTextArea(){
        return simulationTextArea;
    }

    public void setRows(String[][] rows) {
        GraphicalInterface.rows = rows;
    }
}